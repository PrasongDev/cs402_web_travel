from django.shortcuts import render, redirect
from django.utils import translation
import re
from rest_framework.viewsets import ModelViewSet
from .serializers import CategoryMainSerializer, CategorySubSerializer, LocationSerializer, LocationGallerySerializer, TripSerializer, StyleSerializer
from location.models import CategoryMain, CategorySub, Location, LocationGallery
from trip.models import Trip, Style

# ========================== LOCATION ==========================
class CategoryMainViewSet(ModelViewSet):
    queryset = CategoryMain.objects.all()
    serializer_class = CategoryMainSerializer

class CategorySubViewSet(ModelViewSet):
    queryset = CategorySub.objects.all()
    serializer_class = CategorySubSerializer

class LocationViewSet(ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

class LocationGalleryViewSet(ModelViewSet):
    queryset = LocationGallery.objects.all()
    serializer_class = LocationGallerySerializer
# ========================== END LOCATION ==========================


# ========================== TRIP ==========================
class StyleViewSet(ModelViewSet):
    queryset = Style.objects.all()
    serializer_class = StyleSerializer

class TripViewSet(ModelViewSet):
    queryset = Trip.objects.all()
    serializer_class = TripSerializer
# ========================== END TRIP ==========================

# ========================== OTHER ==========================
def changeLanguage(request, language):
    translation.activate(language)
    request.session[translation.LANGUAGE_SESSION_KEY] = language
    oldUrl = request.META['HTTP_REFERER']
    newUrl = ''
    if re.search('/en/', oldUrl):
        newUrl = oldUrl.replace('/en/', '/th/')
    else: 
        newUrl = oldUrl.replace('/th/', '/en/')
    return redirect(newUrl)
# ========================== END OTHER ==========================

