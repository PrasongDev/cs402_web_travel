from django import forms

from blog.models import BlogPost 
from django.utils.translation import ugettext_lazy as _


class CreateBlogPostForm(forms.ModelForm):

	class Meta:
		model = BlogPost
		fields = ['title', 'tags', 'body', 'image',]
		widgets = {
            'tags':forms.TextInput(attrs={'data-role': 'tagsinput', 'value': "ประวัติศาสตร์"}),
        }
        # labels = {
        #     'name': _('Writer'),
        # }
        # help_texts = {
        #     'name': _('Some useful help text.'),
        # }
        # error_messages = {
        #     'name': {
        #         'max_length': _("This writer's name is too long."),
        #     },
        # }

class UpdateBlogPostForm(forms.ModelForm):

	class Meta:
		model = BlogPost
		fields = ['title','tags', 'body', 'image']
		widgets = {
            'tags': forms.TextInput(attrs={'data-role': 'tagsinput'})
        }

	# def save(self, commit=True):
	# 	blog_post = self.instance
	# 	blog_post.title = self.cleaned_data['title']
	# 	blog_post.tags = self.cleaned_data['tags']
	# 	blog_post.body = self.cleaned_data['body']

	# 	if self.cleaned_data['image']:
	# 		blog_post.image = self.cleaned_data['image']

	# 	if commit:
	# 		blog_post.save()
	# 	return blog_post

