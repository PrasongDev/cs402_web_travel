# https://developers.google.com/optimization/routing
# https://developers.google.com/maps/documentation/geocoding/intro

"""Simple travelling salesman problem between cities."""

from __future__ import print_function
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp


# distance_list = ['โรงพยาบาลธรรมศาสตร์เฉลิมพระเกียรติ', 'น้ำตกนางครวญ', 'น้ำตกเอราวัณ', 'อุทยานแห่งชาติเขาแหลม', 'ช่องเขาขาด', 'วัดบ้านถ้ำ', 'วัดถ้ำเสือ', 'รักษ์คันนา']


# def create_data_model(data):
#     """Stores the data for the problem."""
#     data = {}
#     data['distance_matrix'] = [
#         [0, 316585, 210889, 322986, 228663, 137573, 825402, 134135], [317961, 0, 163789, 31424, 90465, 185103, 958568, 194296], [212383, 163996, 0, 170398, 76075, 79548, 859267, 88718], [324362, 31424, 170191, 0, 96866, 191505, 964970, 200698], [230039, 90465, 75867, 96866, 0, 97181, 870646, 106374], [140538, 185143, 79465, 191545, 97222, 0, 772670, 4709], [824430, 957712, 858933, 964113, 869790, 772467, 0, 769944], [137100, 194346, 88650, 200748, 106424, 4709, 771568, 0]
#     ]  # yapf: disable
#     data['num_vehicles'] = 1
#     data['depot'] = 0
#     return data


def print_solution(manager, routing, assignment, distance_list):
    """Prints assignment on console."""
    print('Objective: {} miles'.format(assignment.ObjectiveValue()))
    index = routing.Start(0)

    plan_output = 'Route for vehicle 0:\n'
    plan_order = []
    plan_list = []
    route_distance = 0
    
    while not routing.IsEnd(index):
        plan_output += ' {} ->'.format(distance_list[manager.IndexToNode(index)])
        plan_order.append(distance_list[manager.IndexToNode(index)])
        previous_index = index
        index = assignment.Value(routing.NextVar(index))
        route_distance += routing.GetArcCostForVehicle(
            previous_index, index, 0)
    plan_output += ' {}\n'.format(distance_list[manager.IndexToNode(index)])
    plan_order.append(distance_list[manager.IndexToNode(index)])
    # print(plan_output)
    plan_output += 'Route distance: {}miles\n'.format(route_distance)

    plan_list.append(plan_order)
    plan_list.append(route_distance)
    return plan_list
    # return plan_order


# def main():
#     """Entry point of the program."""
#     # Instantiate the data problem.
#     data = create_data_model()

#     # Create the routing index manager.
#     manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
#                                            data['num_vehicles'], data['depot'])

#     # Create Routing Model.
#     routing = pywrapcp.RoutingModel(manager)

#     def distance_callback(from_index, to_index):
#         """Returns the distance between the two nodes."""
#         # Convert from routing variable Index to distance matrix NodeIndex.
#         from_node = manager.IndexToNode(from_index)
#         to_node = manager.IndexToNode(to_index)
#         return data['distance_matrix'][from_node][to_node]

#     transit_callback_index = routing.RegisterTransitCallback(distance_callback)

#     # Define cost of each arc.
#     routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

#     # Setting first solution heuristic.
#     search_parameters = pywrapcp.DefaultRoutingSearchParameters()
#     search_parameters.first_solution_strategy = (
#         routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

#     # Solve the problem.
#     assignment = routing.SolveWithParameters(search_parameters)

#     # Print solution on console.
#     if assignment:
#         print_solution(manager, routing, assignment)


# if __name__ == '__main__':
#     main()
