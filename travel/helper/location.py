from location.models import CategorySub, Location, LocationGallery
import json, time, datetime
# from datetime import datetime
from datetime import datetime, timedelta

# ================================ Function ================================

def get_category_id_return_category_name(category_pk):
    category = CategorySub.objects.filter(pk=category_pk)
    category = category[0]
    return category.name

def get_location_return_cost(location_name):
    location = Location.objects.filter(name = location_name)
    # location = Location.objects.get(id = 3)
    if not location:
        cost = 0
    else:
        cost = location[0].cost

    return cost

def get_location_return_id(location_name):
    location = Location.objects.filter(name = location_name)
    return location[0].id


def get_location_return_object_image(location_name):
    # print('===============get_location_return_object_image===============')
    # print(location_name)
    image = LocationGallery.objects.filter(location_gallery = get_location_return_id(location_name))
    if not image:
        pass
    else:
        # print(image)    
        return image


def get_arr_time(seconds):
    seconds_in_day = 86400
    seconds_in_hour = 3600
    seconds_in_minute = 60

    days = seconds // seconds_in_day
    seconds = seconds - (days * seconds_in_day)

    hours = seconds // seconds_in_hour
    seconds = seconds - (hours * seconds_in_hour)

    minutes = seconds // seconds_in_minute
    seconds = seconds - (minutes * seconds_in_minute)

    time_list = []

    if(days == 0):
        time_list.append(hours)
        time_list.append(minutes)
        time_list.append(seconds)
    else:
        time_list.append(days)
        time_list.append(hours)
        time_list.append(minutes)
        time_list.append(seconds)

    return time_list

def get_time(seconds, location_number):
    seconds_in_day = 86400
    seconds_in_hour = 3600
    seconds_in_minute = 60

    seconds = seconds + (location_number * seconds_in_hour)

    # seconds = int(input("Enter a number of seconds: "))

    days = seconds // seconds_in_day
    seconds = seconds - (days * seconds_in_day)

    hours = seconds // seconds_in_hour
    seconds = seconds - (hours * seconds_in_hour)

    minutes = seconds // seconds_in_minute
    seconds = seconds - (minutes * seconds_in_minute)

    if(days == 0):
        # hours, minutes, seconds
        time_use = []
        time_use.append(hours)
        time_use.append(minutes)
        time_use.append(seconds)
        return time_use
    else:
        # days, hours, minutes, seconds
        time_use = []
        time_use.append(days)
        time_use.append(hours)
        time_use.append(minutes)
        time_use.append(seconds)
        return time_use
        


def get_list_id_category_return_list_name_category(request):

    category_list = []
    for item in request:
        category = CategorySub.objects.filter(pk = item)
        category_list.append(category[0].name)

    return category_list



def get_location_name_return_location_object(location_name):
    location = Location.objects.filter(name = location_name)
    if not location:
        return ""
    else :    
        location = location[0]
        return location



def get_location_name_return_location_cost(location_name):
    location = Location.objects.filter(name = location_name)
    if not location:
        return ""
    else :    
        location = location[0]
        return location.cost
    


def get_location_list_name_return_name_and_cost_to_json(lication_list_name):

    count = 0
    json_format = '{ "location_cost" : [ '
    for item in lication_list_name:
        location = Location.objects.filter(name = item)
        location = location[0]
        location.cost
        json_format += '{"name": ' + "\"" + str(location.name) + "\"" + ','
        json_format += '"cost": ' + "\"" + str(location.cost) + "\"" + '}'
        if count != len(lication_list_name)-1 :
            json_format += ', '
        count += 1
    json_format += ' ] }'

    return json_format


def seconds_interval(start, end):
    seconds_in_day = 86400
    seconds_in_hour = 3600
    seconds_in_minute = 60
    seconds = (end.hour + start.hour)*seconds_in_hour + (end.minute + start.minute)*seconds_in_minute + (end.second + start.second)
    
    days = seconds // seconds_in_day
    seconds = seconds - (days * seconds_in_day)

    hours = seconds // seconds_in_hour
    seconds = seconds - (hours * seconds_in_hour)

    minutes = seconds // seconds_in_minute
    seconds = seconds - (minutes * seconds_in_minute)


    return datetime.time(hours, minutes, seconds)



def get_duration_list_return_duration_value(duration_list):
    detail_value = []
    count = 0
    for datail in duration_list:
        detail_value.append(duration_list[count][1][0][0])
        count += 1

    return detail_value



def get_data_return_json(location_list, duration_list , distance_list, time_start, location_time_list, time_hotel):

    trip_description = "{ "

    # =========================== trip_overview ===========================
    trip_overview_detail = []

    cost_all = 0
    for location in location_list:
        cost_all += get_location_return_cost(location)
    trip_overview_detail.append(cost_all)

    duration_all = 0
    distance_all = 0
    count = 0
    for datail in location_list:
        if count < len(location_list)-1:
            per = count
            pre = count + 1
            duration_all += duration_list[count][1][0][0]
            distance_all += distance_list[count][1][0][0]
            if(count < len(location_list) ):
                count += 1

    trip_overview_detail.append(duration_all)
    trip_overview_detail.append(distance_all)

    trip_overview = '"trip_overview" : [ '
    trip_overview += '{"cost_all": ' + "\"" + str(trip_overview_detail[0]) + "\"" + ','
    trip_overview += '"duration_all": ' + "\"" + str(trip_overview_detail[1]) + "\"" + ','
    trip_overview += '"distance_all": ' + "\"" + str(trip_overview_detail[2]) + "\"" + '}'
    trip_overview += ' ]'

    trip_description += trip_overview
    trip_description += " , "

    # =========================== location_between ===========================
    location_between_detail = []

    # cost
    cost_list = []
    for location in location_list:
        cost_list.append(get_location_return_cost(location))

    detail_text = []
    count = 0
    for datail in location_list:
        if count < len(location_list)-1:
            per = count
            pre = count + 1
            detail_text.append("{} ไป {}".format(location_list[per], location_list[pre]))
            # detail_text.append(cost_list[count])
            detail_text.append(duration_list[count][0][0][0])
            detail_text.append(duration_list[count][1][0][0])
            detail_text.append(distance_list[count][0][0][0])
            detail_text.append(distance_list[count][1][0][0])
            location_between_detail.append(detail_text)
            detail_text = []
            detail_value = []
            if(count < len(location_list) ):
                count += 1

    count = 0
    location_between = '"location_between" : [ '
    for item in location_between_detail:
        location_between += '{"id": ' + "\"" + str(count) + "\"" + ','
        location_between += '"location": ' + "\"" + str(item[0]) + "\"" + ','
        location_between += '"duration": ' + "\"" + str(item[1]) + "\"" + ','
        location_between += '"duration_value": ' + "\"" + str(item[2]) + "\"" + ','
        location_between += '"distance": ' + "\"" + str(item[3]) + "\"" + ','
        location_between += '"distance_value": ' + "\"" + str(item[4]) + "\"" + '}'

        if count != len(location_between_detail)-1 :
            location_between += ', '
        count += 1
    location_between += ' ] '


    trip_description += location_between
    trip_description += " , "

    # =========================== location_time ===========================
    duration_value = []
    count = 0
    for datail in duration_list:
        duration_value.append(duration_list[count][1][0][0])
        count += 1

    print('time_start: {}'.format(time_start))
    print('time_hotel: {}'.format(time_hotel))

    itme_date = 1
    time_split = time_hotel.split(":") 
    time_hotel = timedelta(hours = int(time_split[0]), minutes = int(time_split[1]), seconds = int(time_split[2]))
    # time_hotel = datetime.time(int(time_split[0]), int(time_split[1]), int(time_split[2]))
    # time_delay = datetime.time(1, 0, 0)
    time_split = time_start.split(":")
    time_start = timedelta(hours = int(time_split[0]), minutes = int(time_split[1]), seconds = int(time_split[2]))
    # time_start = datetime.time(int(time_split[0]), int(time_split[1]), int(time_split[2]))
    # print(location_list)
    # print(duration_value)

    count = 0
    location_time = '"location_time" : [ '
    for item in location_list:
        if count != 0:
            time_1 = get_arr_time(duration_value[count-1])
            print(time_1)
            time_2 = timedelta(hours = time_1[0], minutes = time_1[1], seconds = time_1[2])
            # time_2 = datetime.time(time_1[0], time_1[1], time_1[2])
            print(time_2)
            # print("time_2: {}".format(time_2))


            xyz = location_list[count]
            matching = [s for s in location_time_list if xyz in s[0]]
            print(matching)
            if matching:
                time_delay = timedelta(hours=0, minutes = matching[0][1], seconds=0)
                # matching_arr = str(matching[0][1]).split(":")
                # time_delay = datetime.time(int(matching_arr[0]), int(matching_arr[1]), int(matching_arr[2]))
            
            # print(time_2)
            # print(time_delay)
            # print(time_2 + time_delay)

            time_all = time_delay + time_2
            # time_all = seconds_interval(time_delay, time_2)

            if (time_all + time_start) <=  time_hotel:
            # if seconds_interval(time_all, time_start) <=  time_hotel:
                time_start = (time_all + time_start)
                # time_start = seconds_interval(time_all, time_start)
                # print("time_start: {}".format(time_start))
            else:
                itme_date += 1
                time_start = timedelta(hours=9, minutes = 0, seconds=0)
                # time_start = datetime.time(9, 0, 0)
                location_time += '{"date": ' + "\"" + str(itme_date) + "\"" + ','
                location_time += '"location": ' + "\"" + str("เข้าที่พัก") + "\"" + ','
                location_time += '"time": ' + "\"" + str(time_hotel) + "\"" + '},'

                location_time += '{"date": ' + "\"" + str(itme_date) + "\"" + ','
                location_time += '"location": ' + "\"" + str("ออกจากที่พัก") + "\"" + ','
                location_time += '"time": ' + "\"" + str(time_start) + "\"" + '},'
                time_start = time_all + time_start
                # time_start = seconds_interval(time_all, time_start)


        location_time += '{"date": ' + "\"" + str(itme_date) + "\"" + ','
        location_time += '"location": ' + "\"" + location_list[count] + "\"" + ','
        location_time += '"time": ' + "\"" + str(time_start) + "\"" + '}'

        if count != len(location_list)-1 :
            location_time += ', '
        count += 1

    location_time += ' ] '

    trip_description += location_time
    trip_description += " , "


    # =========================== location_order ===========================
    # count = 0
    # location_order = '"location_order" : [ '
    # for item in location_list:
    #     location_order += '{"order": ' + "\"" + str(count) + "\"" + ','
    #     location_order += '"name": ' + "\"" + item + "\"" + ','
    #     location_order += '"cost": ' + "\"" + str(get_location_name_return_location_cost(item)) + "\"" + '}'

    #     if count != len(location_list)-1 :
    #         location_order += ', '
    #     count += 1
    # location_order += ' ] '

    # trip_description += location_order
    # trip_description += " , "


    # =========================== location_time_order ===========================

    count = 0
    location_order = '"location_order" : [ '
    for item in location_time_list:
        location_order += '{"order": ' + "\"" + str(count) + "\"" + ','
        location_order += '"name": ' + "\"" + str(item[0]) + "\"" + ','
        location_order += '"cost": ' + "\"" + str(get_location_name_return_location_cost(item[0])) + "\"" + ','
        location_order += '"time": ' + "\"" + str(item[1]) + "\"" + '}'

        if count != len(location_time_list)-1 :
            location_order += ', '
        count += 1
    location_order += ' ] '

    trip_description += location_order
    trip_description += "}"

    return trip_description



def get_location_return_category(location_name):
    location = Location.objects.filter(name = location_name)
    if location:
        location = location[0]
        return location.category_sub
    else:
        return ""

# ================================ End Function ================================


def load_location_start_end():
    json_file = open('static/json/provinces.json', 'r')
    objs = json.load(json_file)
    location_start = []
    for obj in objs:
        location_start.append(obj["PROVINCE_NAME"])
    
    return location_start
    
