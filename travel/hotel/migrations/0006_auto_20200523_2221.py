# Generated by Django 3.0.5 on 2020-05-23 15:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hotel', '0005_auto_20200523_2221'),
    ]

    operations = [
        migrations.RenameField(
            model_name='room',
            old_name='View',
            new_name='overview',
        ),
    ]
