from django.conf.urls import url
from django.urls import include, path
from hotel.views import hotel, reservation

# app_name = 'hotel'

urlpatterns = [

     path('', hotel.hotelSearch.as_view(), name='home_hotel'),


     path('', include(([
          path('', hotel.hotelindex, name='hotel_view'),
          path('hotel/(<pk>[0-9]+)/', hotel.hoteldetails, name='hotel_detail'),
          path('hotel/search', hotel.hotelSearch.as_view(), name='hotel_search'),
          path('reviews/create/(<id>[0-9]+)', hotel.reviewCreateView.as_view(), name='review_create'),
          path('reviews/update/(<pk>[0-9]+)', hotel.reviewUpdateView.as_view(), name='review_update'),
          path('reviews/delete/(<pk>[0-9]+)', hotel.reviewDeleteView.as_view(), name='review_delete'),
          path('partner/apply', hotel.partnerCreateView.as_view(), name='proposal_create'),
    ], 'hotel'), namespace='hotel')),


     path('reservation/', include(([
          path('book/(<hotelid>[0-9]+)/(<roomid>[0-9]+)', reservation.bookRoom, name='bookroom'),
          path('book/create/(<roomid>[0-9]+)/(<hotelid>[0-9]+)/(<checkin>(\d{4}-\d{2}-\d{2}))/(<checkout>(\d{4}-\d{2}-\d{2}))/(<totalcost>[0-9]+)'
          , reservation.booking_create, name='booking_create'),

          path('mybookings/', reservation.mybookings, name='bookings_view'),
          path('mybookings/cancel/(<id>[0-9]+)', reservation.cancelbooking, name='booking_cancel'),
          path('mybookings/(<id>[0-9]+)/pdf', reservation.GeneratePDF.as_view(), name='gpdf'),
    ], 'hotel'), namespace='reservation')),

]
