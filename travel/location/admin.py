from django.contrib import admin
from location.models import CategoryMain, CategorySub, Location, LocationGallery


# Location
class LocationInline(admin.TabularInline):
    model = Location
    # readonly_fields=('pk','id',)
    extra = 3
    # list_display = [field.name for field in Location._meta.fields]

class LocationAdmin(admin.ModelAdmin):
    fieldsets = [('Date Information', {'fields': ['name',]}),]
    inlines = [LocationInline]

admin.site.register(CategorySub, LocationAdmin)


# Category
class CategorySubInline(admin.TabularInline):
    model = CategorySub
    extra = 3

class CategorySubAdmin(admin.ModelAdmin):
    fieldsets = [('Date Information', {'fields': ['name',]}),]
    inlines = [CategorySubInline]

admin.site.register(CategoryMain, CategorySubAdmin)


# Image
class ImagesInline(admin.TabularInline):
    model = LocationGallery

class GalleryAdmin(admin.ModelAdmin):
    inlines = [ImagesInline]

admin.site.register(Location, GalleryAdmin) 
