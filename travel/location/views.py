from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView

from location.models import Location, LocationGallery
from location.forms import LocationCreateForm
from location.filters import LocationFilter
from helper.location import get_location_return_object_image

# Search
def search(request):
    location_list = Location.objects.all()
    location_filter = LocationFilter(request.GET, queryset=location_list)

    data = {
        # 'max': max,
        # 'min': min,
        # 'cost_list': cost_list,
        'filter': location_filter,
    }
    
    return render(request, 'location/location_search.html', data)



class LocationView(ListView):
    template_name = 'location/location_list.html'
    context_object_name = 'location_image'
    model = LocationGallery



def location_list(request):

    locations = Location.objects.all()

    # google = Location.objects.get(name='น้ำตกเอราวัณ')
    # location_image = google.location_image.all()
    # print(location_image)

    
    locations_images = []
    images_list = LocationGallery.objects.all()
    # for item in images_list:
    #     locations_images.append(item)

    # for item in locations:
    #     location_line = []
    #     location_line.append(item.name)

    # for item in locations:
    #     # location_line = []
    #     location_line = LocationGallery.objects.filter(location_gallery=item)
    #     if not location_line[0]:
    #         locations_images.append("null")
    #     else:    
    #         locations_images.append(location_line[0])

    location_list = []
    for item in locations:
        location_line = []
        location_line.append(item)
        location_line.append(LocationGallery.objects.filter(location_gallery=item))
        location_list.append(location_line)


    data = {
        'locations': locations,
        'location_list': location_list,
    }

    return render(request, "location/location_list.html", data)



def LocationDetail(request, pk):
    location = Location.objects.filter(pk=pk)
    location = location[0]

    images = get_location_return_object_image(location)

    data = {
        'location': location,
        'images': images,
    }

    return render(request, 'location/location_detail.html', {'data':data})

