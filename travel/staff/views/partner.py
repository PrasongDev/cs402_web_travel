from django.shortcuts import render
from django.http import HttpResponse
# from django.core.urlresolvers import reverse,reverse_lazy
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from hotel.models import Proposal
from account.models import Partners, Account

# Displays the appropriate dashboard for each type of user.


def displayDash(request):
    user = request.user
    if user.account.roleid == 3:
        link = reverse('HotelApp:hotelindex')
        return HttpResponseRedirect(link)
    elif user.account.roleid == 4:
        link = reverse('ManageHotels:home')
        return HttpResponseRedirect(link)
    elif user.account.roleid == 2:
        link = reverse('hotel_admin:home')
        return HttpResponseRedirect(link)


# Show partners , Accept proposals and remove partners.
def showProposals(request):
    proposal_list = Proposal.objects.all()
    context = {'proposals': proposal_list}
    return render(request, 'staff/partner/proposals.html', context)


def showPartners(request):
    partner_list = Partners.objects.all()
    context = {'partners': partner_list}
    return render(request, 'staff/partner/partners_view.html', context)


def removePartner(request, id):
    partner = Partners.objects.get(id=id)
    userid = partner.userID
    role = Account.objects.get(user=userid)
    role.roleid = 3
    role.save()
    partner.delete()

    link = reverse('Authorize:showpartners')
    return HttpResponseRedirect(link)
    # Accepts a partner and inputs them into the partners database aswell
    # as changing the role id to reflect this .


def acceptProposals(request, id):
    proposal = Proposal.objects.get(id=id)
    user = proposal.user
    newpartner = Partners()
    newpartner.userID = user
    newpartner.CompanyName = proposal.CompanyName
    newpartner.CompanyEmail = proposal.CompanyEmail
    newpartner.HQAddress = proposal.HQAddress
    newpartner.save()
    updaterole = Account.objects.get(user=user)
    updaterole.roleid = 4
    updaterole.save()
    proposal.delete()
    link = reverse('Authorize:showproposals')
    return HttpResponseRedirect(link)


def declineProposals(request, id):
    proposal = Proposal.objects.get(id=id)
    proposal.delete()
    link = reverse('Authorize:showproposals')
    return HttpResponseRedirect(link)

#  A user can check the status of their application


def checkstatus(request):
    proposal_list = Proposal.objects.filter(user=request.user)
    context = {'proposal': proposal_list}
    return render(request, 'staff/partner/partners_check.html', context)
