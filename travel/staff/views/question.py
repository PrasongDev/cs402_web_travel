from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponse
from django.db.models import Avg, Count
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView)
from account.models import Account
from question.decorators import staff_required
from question.forms import BaseAnswerInlineFormSet, QuestionForm, StaffSignUpForm
from question.models import Subject, Active, Question, TakenQuiz, Quiz, Answer

import csv, json


@method_decorator([login_required, staff_required], name='dispatch')
class QuizListView(ListView):
    model = Quiz
    ordering = ('name', )
    context_object_name = 'quizzes'
    template_name = 'staff/question/quiz_change_list.html'

    def get_queryset(self):
        queryset = self.request.user.quizzes \
            .select_related('subject') \
            .annotate(questions_count=Count('questions', distinct=True)) 
            # .annotate(taken_count=Count('taken_quizzes', distinct=True))
        return queryset


@method_decorator([login_required, staff_required], name='dispatch')
class QuizCreateView(CreateView):
    model = Quiz
    fields = ('name', 'subject', )
    template_name = 'staff/question/quiz_add_form.html'

    def form_valid(self, form):
        quiz = form.save(commit=False)
        quiz.owner = self.request.user
        quiz.save()
        messages.success(self.request, 'The quiz was created with success! Go ahead and add some questions now.')
        return redirect('question_admin:quiz_change', quiz.pk)


@method_decorator([login_required, staff_required], name='dispatch')
class QuizUpdateView(UpdateView):
    model = Quiz
    fields = ('name', 'subject', )
    context_object_name = 'quiz'
    template_name = 'staff/question/quiz_change_form.html'

    def get_context_data(self, **kwargs):
        kwargs['questions'] = self.get_object().questions.annotate(answers_count=Count('answers'))
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        '''
        This method is an implicit object-level permission management
        This view will only match the ids of existing quizzes that belongs
        to the logged in user.
        '''
        return self.request.user.quizzes.all()

    def get_success_url(self):
        return reverse('question_admin:quiz_change', kwargs={'pk': self.object.pk})


@method_decorator([login_required, staff_required], name='dispatch')
class QuizDeleteView(DeleteView):
    model = Quiz
    context_object_name = 'quiz'
    template_name = 'staff/question/quiz_delete_confirm.html'
    success_url = reverse_lazy('question_admin:quiz_change_list')

    def delete(self, request, *args, **kwargs):
        quiz = self.get_object()
        messages.success(request, 'The quiz %s was deleted with success!' % quiz.name)
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.quizzes.all()





@method_decorator([login_required, staff_required], name='dispatch')
class QuizResultsView(DetailView):
    model = Quiz
    context_object_name = 'quiz'
    template_name = 'staff/question/quiz_results.html'

    def get_context_data(self, **kwargs):
        quiz = self.get_object()
        taken_quizzes = quiz.taken_quizzes.select_related('active__user').order_by('-date')
        total_taken_quizzes = taken_quizzes.count()
        quiz_score = quiz.taken_quizzes.aggregate(average_score=Avg('score'))
        extra_context = {
            'taken_quizzes': taken_quizzes,
            'total_taken_quizzes': total_taken_quizzes,
            'quiz_score': quiz_score
        }
        kwargs.update(extra_context)
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        return self.request.user.quizzes.all()


@login_required
@staff_required
def question_add(request, pk):
    # By filtering the quiz by the url keyword argument `pk` and
    # by the owner, which is the logged in user, we are protecting
    # this view at the object-level. Meaning only the owner of
    # quiz will be able to add questions to it.
    quiz = get_object_or_404(Quiz, pk=pk, owner=request.user)

    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.quiz = quiz
            question.save()
            messages.success(request, 'You may now add answers/options to the question.')
            return redirect('question_admin:question_change', quiz.pk, question.pk)
    else:
        form = QuestionForm()

    return render(request, 'staff/question/question_add_form.html', {'quiz': quiz, 'form': form})


@login_required
@staff_required
def question_change(request, quiz_pk, question_pk):
    # Simlar to the `question_add` view, this view is also managing
    # the permissions at object-level. By querying both `quiz` and
    # `question` we are making sure only the owner of the quiz can
    # change its details and also only questions that belongs to this
    # specific quiz can be changed via this url (in cases where the
    # user might have forged/player with the url params.
    quiz = get_object_or_404(Quiz, pk=quiz_pk, owner=request.user)
    question = get_object_or_404(Question, pk=question_pk, quiz=quiz)

    AnswerFormSet = inlineformset_factory(
        Question,  # parent model
        Answer,  # base model
        formset=BaseAnswerInlineFormSet,
        fields=('text', 'is_correct'),
        min_num=2,
        validate_min=True,
        max_num=10,
        validate_max=True
    )

    if request.method == 'POST':
        form = QuestionForm(request.POST, instance=question)
        formset = AnswerFormSet(request.POST, instance=question)
        if form.is_valid() and formset.is_valid():
            with transaction.atomic():
                form.save()
                formset.save()
            messages.success(request, 'Question and answers saved with success!')
            return redirect('question_admin:quiz_change', quiz.pk)
    else:
        form = QuestionForm(instance=question)
        formset = AnswerFormSet(instance=question)

    return render(request, 'staff/question/question_change_form.html', {
        'quiz': quiz,
        'question': question,
        'form': form,
        'formset': formset
    })


@method_decorator([login_required, staff_required], name='dispatch')
class QuestionDeleteView(DeleteView):
    model = Question
    context_object_name = 'question'
    template_name = 'staff/question/question_delete_confirm.html'
    pk_url_kwarg = 'question_pk'

    def get_context_data(self, **kwargs):
        question = self.get_object()
        kwargs['quiz'] = question.quiz
        return super().get_context_data(**kwargs)

    def delete(self, request, *args, **kwargs):
        question = self.get_object()
        messages.success(request, 'The question %s was deleted with success!' % question.text)
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return Question.objects.filter(quiz__owner=self.request.user)

    def get_success_url(self):
        question = self.get_object()
        return reverse('question_admin:quiz_change', kwargs={'pk': question.quiz_id})


def generate_report(request):

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="result.csv"'

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)
    
    # mls = ML.objects.all()
    # writer.writerow(['Active', 'Subject', 'Quiz', 'Question', 'Answer'])      # First row
    # for item in mls:
    #     writer.writerow([item.active, item.subject, item.quiz, item.question, item.answer])
    # question_list = []
    # question_list.append('Active')
    # for item in mls:
    #     question_list.append(item.question)
    # writer.writerow(question_list)      # First row

    # mls = ML.objects.all()

    # answer_name = ""
    # name_list = []
    # question_process = []
    # answer_list = []
    # answer_process = []    
    # question_line = []
    # question_list = []
    # count_test = 0

    # for item in mls:
    #     if(answer_name == ""):
    #         answer_name = item.active
    #         name_list.append(answer_name)
    #     elif answer_name != item.active:
    #         question_process.append("class")
        
    #         count_hard = 0
    #         count_soft = 0
    #         count_non = 0
        
    #         for answer_1 in answer_process:
    #             answer_2 = (str(answer_1).split(', ')) 
    #             if(len(answer_2) > 1):
    #                 # print(answer_2)

    #                 for answer_3 in answer_2:
    #                     if answer_3 == "Hard":
    #                         count_hard += 1
    #                         # print(answer_3)
    #                     elif answer_3 == "Non-adventure":
    #                         count_non += 1
    #                         # print(answer_3)
    #                     elif answer_3 == "Soft" :
    #                         count_soft += 1
    #                         # print(answer_3)
    #             # print(("answer_1: {}").format(answer_1))
                         
    #         if(count_hard > count_soft and count_hard > count_non):
    #             answer_process.append("Hard")
    #         if(count_soft > count_hard and count_soft > count_non):
    #             answer_process.append("Soft")
    #         if(count_non > count_soft and count_non > count_hard):
    #             answer_process.append("Non-adventure")
    #         question_list.append(question_process)
    #         answer_list.append(answer_process)
    #         question_process = []
    #         answer_process = []
    #         answer_name = item.active
    #         name_list.append(answer_name)
        
    #     if answer_name == item.active:
    #         question_process.append(item.question)
    #         answer_process.append(item.answer)
    #         count_test += 1

    #     if count_test == len(mls):
    #         question_process.append("class")
        
    #         count_hard = 0
    #         count_soft = 0
    #         count_non = 0
        
    #         for answer_1 in answer_process:
    #             answer_2 = (str(answer_1).split(', ')) 
    #             if(len(answer_2) > 1):
    #                 # print(answer_2)

    #                 for answer_3 in answer_2:
    #                     if answer_3 == "Hard":
    #                         count_hard += 1
    #                         # print(answer_3)
    #                     elif answer_3 == "Non-adventure":
    #                         count_non += 1
    #                         # print(answer_3)
    #                     elif answer_3 == "Soft" :
    #                         count_soft += 1
    #                         # print(answer_3)
    #             # print(("answer_1: {}").format(answer_1))
                         
    #         # Non-adventure
    #         if(count_non > count_soft and count_non > count_hard):
    #             answer_process.append("0")        
    #         # Soft
    #         if(count_soft > count_hard and count_soft > count_non):
    #             answer_process.append("1")
    #         # Hard
    #         if(count_hard > count_soft and count_hard > count_non):
    #             answer_process.append("2")

    #         question_list.append(question_process)
    #         answer_list.append(answer_process)
    #         question_process = []
    #         answer_process = []
        
    # print(question_list)
    # print(answer_list)

    # head = []
    # for item in question_list:
    #     if(len(item) > len(head)):
    #         head = item

    # writer.writerow(head)      
    # for line in answer_list:
    #     writer.writerow(line)

    return response


@login_required
@staff_required
def results_view(request):
    
    mls = ML.objects.all()

    answer_name = ""
    name_list = []
    question_process = []
    answer_list = []
    answer_process = []    
    question_line = []
    question_list = []
    count_test = 0

    for item in mls:
        if(answer_name == ""):
            answer_name = item.active
            name_list.append(answer_name)
        elif answer_name != item.active:
            question_process.append("class")
        
            count_hard = 0
            count_soft = 0
            count_non = 0
        
            for answer_1 in answer_process:
                answer_2 = (str(answer_1).split(', ')) 
                if(len(answer_2) > 1):
                    # print(answer_2)

                    for answer_3 in answer_2:
                        if answer_3 == "Hard":
                            count_hard += 1
                            # print(answer_3)
                        elif answer_3 == "Non-adventure":
                            count_non += 1
                            # print(answer_3)
                        elif answer_3 == "Soft" :
                            count_soft += 1
                            # print(answer_3)
                # print(("answer_1: {}").format(answer_1))

            # Non-adventure
            if(count_non > count_soft and count_non > count_hard):
                answer_process.append("Non-adventure")        
            # Soft
            if(count_soft > count_hard and count_soft > count_non):
                answer_process.append("Soft")
            # Hard
            if(count_hard > count_soft and count_hard > count_non):
                answer_process.append("Hard")

            question_list.append(question_process)
            answer_list.append(answer_process)
            question_process = []
            answer_process = []
            answer_name = item.active
            name_list.append(answer_name)
        
        if answer_name == item.active:
            question_process.append(item.question)
            answer_process.append(item.answer)
            count_test += 1

        if count_test == len(mls):
            question_process.append("class")
        
            count_hard = 0
            count_soft = 0
            count_non = 0
        
            for answer_1 in answer_process:
                answer_2 = (str(answer_1).split(', ')) 
                if(len(answer_2) > 1):
                    # print(answer_2)

                    for answer_3 in answer_2:
                        if answer_3 == "Hard":
                            count_hard += 1
                            # print(answer_3)
                        elif answer_3 == "Non-adventure":
                            count_non += 1
                            # print(answer_3)
                        elif answer_3 == "Soft" :
                            count_soft += 1
                            # print(answer_3)
                # print(("answer_1: {}").format(answer_1))
                         
            if(count_hard > count_soft and count_hard > count_non):
                answer_process.append("Hard")
            if(count_soft > count_hard and count_soft > count_non):
                answer_process.append("Soft")
            if(count_non > count_soft and count_non > count_hard):
                answer_process.append("Non-adventure")

            question_list.append(question_process)
            answer_list.append(answer_process)
            question_process = []
            answer_process = []
        
    # print(question_list)
    # print(answer_list)

    head = []
    for item in question_list:
        if(len(item) > len(head)):
            head = item

    # result_file = pd.read_csv("static/Result-5.csv")


    data = {
        'head': head,
        'answer_list': answer_list,
    }

    return render(request, "staff/question/quiz_results_list.html", data)
