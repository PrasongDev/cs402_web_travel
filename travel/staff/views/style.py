from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView

from trip.models import Style


class StyleCreateView(CreateView):
    model = Style
    context_object_name = 'style'
    template_name = 'staff/style/style_create.html'
    fields = ['name', ]
    # success_url = reverse_lazy('other')
    def get_success_url(self):
        return reverse_lazy('other_staff')


class StyleUpdateView(UpdateView):
    model = Style
    context_object_name = 'style'
    template_name = 'staff/style/style_update.html'
    fields = ['name', ]
    # success_url = reverse_lazy('other')
    def get_success_url(self):
        return reverse_lazy('other_staff')


class StyleDeleteView(DeleteView):
    model = Style
    context_object_name = 'style'
    template_name = 'staff/style/style_delete.html'
    # success_url = reverse_lazy('other')
    def get_success_url(self):
        return reverse_lazy('other_staff')

