from django.urls import include, path
from django.conf.urls.static import static
from django.conf import settings

from trip import views

app_name = 'trip'

urlpatterns = [
    

	path('trip_location/', views.trip_location, name="trip_location"),
	path('trip_summary/', views.trip_summary, name="trip_summary"),
	path('update_item/', views.updateItem, name="update_item"),
	path('trip_process/', views.trip_process, name="process_order"),


	path('my_trip/', views.myTrip, name="my_trip"),


    # path('person/', views.PersonCreate, name='trip_person_create'),
    # path('person/save', views.person_save, name='person_save'),
    # path('ajax/load-locations/', views.load_locations, name='ajax_load_locations'),
   
    path('system/', views.system_create, name='trip_system_create'),
    path('system/save/', views.trip_system_save, name='trip_system_save'),


    path('', views.trip_view, name='trip_list'),
    path('search/', views.search, name='search'),
    path('detail/<str:token>/', views.trip_detail, name='trip_detail'),
    path('tag/<slug:slug>/', views.tagged, name="tagged"),
    path('load-map/<str:token>/', views.load_map, name='load_map'),
]