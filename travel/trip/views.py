from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import CreateView, DetailView, ListView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timedelta
from django.utils import translation
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
from django.db.models import Q
from django.template import Context, loader
from taggit.models import Tag
from django.template.defaultfilters import slugify
from django.contrib.auth.decorators import login_required
from sklearn.metrics import classification_report
from question.decorators import active_required
from trip.models import TypeAdventure, TypeSeason, SuitablePerson
from location.models import CategorySub, Location, LocationGallery
from helper.complete_TSP import print_solution
from trip.filters import TripFilter
from helper.map_entire import create_distance_matrix, send_request, build_distance_matrix
from helper.function_knn import encode, encode_data
from helper.function_json import result_table, key_value
from helper.location import (
            get_location_return_cost, 
            get_location_return_id, 
            get_location_return_object_image, 
            get_location_name_return_location_object,
            get_location_list_name_return_name_and_cost_to_json,
            get_time, get_category_id_return_category_name,
            get_list_id_category_return_list_name_category, 
            get_arr_time, seconds_interval, get_data_return_json,
            get_duration_list_return_duration_value,
            get_location_return_category, 
            load_location_start_end,)
from trip.models import Trip, Style, TripLocation
from helper.email import send_html_email
from .utils import location_data, guest_trip
import pandas as pd
import json, secrets, requests, datetime, random, numpy
from django_pandas.io import read_frame


# ================================ KNN ================================
#Import the load_iris function from datsets module
from sklearn.datasets import load_iris
# splitting the data into training and test sets
from sklearn.model_selection import train_test_split
#import the KNeighborsClassifier class from sklearn
from sklearn.neighbors import KNeighborsClassifier
#import metrics model to check the accuracy 
from sklearn import metrics
# ================================ END KNN ================================


def search(request):
    trip_list = Trip.objects.all()
    trip_filter = TripFilter(request.GET, queryset=trip_list)
    data = {
        'filter': trip_filter,
    }

    return render(request, 'trip/trip_search.html', data)


def tagged(request, slug):
    tag = get_object_or_404(Tag, slug=slug)
    common_tags = Trip.tags.most_common()[:4]
    trips = Trip.objects.filter(tags=tag)
    # context = {
    #     'tag':tag,
    #     'common_tags':common_tags,
    #     'trips':trips,
    # }
    data = {
        'tag':tag,
        'trips': trips,
        # 'trip_overview': trip_overview_list,
    } 
    return render(request, 'trip/trip_list.html', { 'data' : data })


# ================================ CRUD ================================


def trip_view(request):
    trips =  Trip.objects.all()
    data = {
        'trips': trips,
    } 
    return render(request, 'trip/trip_list.html', data)


def trip_detail(request, token):
    trips = Trip.objects.filter(token = token)
    # trips = Trip.objects.filter(token = token)
    # persons = get_object_or_404(Person, pk=pk)
    trip = trips[0]

    time_start = str(trip.time_start)
    trip_description_json = json.loads(trip.trip_description_json)
    # location_order = json.loads(trip.location_order)

    person_number = trip.person_number

    # category = trip.category_sub.all()
    style = trip.style
    # style = trip.style.all()

    location_list = []

    location_order_list = trip_description_json["location_order"]
    location_detail = []
    count = 0
    for item in location_order_list:
        # if(count == 0 or count == len(location_order_list)-1):
        #     count += 1
        # else:
            location_list.append(item["name"])
            location_line = []
            location_line.append(get_location_name_return_location_object(item["name"]))
            location_line.append(get_location_name_return_location_object(item["name"]).detail)
            location_detail.append(location_line)
        
            # count += 1

    location_cost_time = []
    for item in location_order_list:
        location_line2 = []
        location_line2.append(item["name"])
        location_line2.append(item["cost"])
        location_line2.append(item["time"])
        location_cost_time.append(location_line2)

    # trip_overview
    cost_all = float(trip_description_json["trip_overview"][0]["cost_all"]) * person_number
    duration_all = get_time(int(trip_description_json["trip_overview"][0]["duration_all"]), len(location_list))
    distance_mi_all = ("{0:.00f}".format(int(trip_description_json["trip_overview"][0]["distance_all"]) / 1609.34395))
    distance_km_all = ("{0:.00f}".format(int(trip_description_json["trip_overview"][0]["distance_all"]) / 1000))


    # location_between
    location_description_list = trip_description_json["location_between"]
    location_description = []
    location_line = []
    duration_list = []
    for item in location_description_list:
        duration_list.append(item["duration_value"])
        location_line.append(item["location"])
        location_line.append(item["duration"])
        location_line.append(item["distance"])
        location_description.append(location_line)
        location_line = []


    image_list = []
    for location_image in location_list:
        image_list.append(get_location_return_object_image(location_image))


    # image_list = []
    # image_line = []
    # for location_image in location_order:
    #     image_line.append(location_image)
    #     image_line.append(get_location_return_object_image(location_image))
    #     image_list.append(image_line)
    

    location_time_list = trip_description_json["location_time"]
    location_time = []
    for item in location_time_list:
        location_line = []
        location_line.append(item["date"])
        location_line.append(item["time"])
        location_line.append(item["location"])
        location_time.append(location_line)


    # location_time_list = trip_description_json["location_time"]
    # location_time = []
    # for item in location_time_list:
    #     location_line2 = []
    #     location_line2.append(item["name"])
    #     location_line2.append(item["cost"])
    #     location_line2.append(item["time"])
    #     location_time.append(location_line2)


    # location_tags = Location.objects.filter(name = )
    location_tags = []
    for item in location_order_list:
        location_line1 = Location.objects.filter(name = item["name"])
        location_line1 = location_line1[0]

        for item_2 in location_line1.tags.all():
            location_tags.append(item_2)

    location_tags = list(set(location_tags))

    data = {
        'trip': trip,
        'cost': cost_all,
        'duration_all': duration_all,
        'distance_mi_all': distance_mi_all,
        'distance_km_all': distance_km_all,
        'location_tags': location_tags,
        'time_start': time_start,
        'location_time': location_time,
        'person_number': person_number, 
        'style': style,
        'location_cost_time': location_cost_time,
        'location_detail': location_detail,
        'location_description': location_description,
        'location_time': location_time,
        'image_list': image_list,
    }
    return render(request, 'trip/trip_detail.html', data)

# ================================ End CRUD ================================


def load_map(request, token):
    trips = Trip.objects.filter(token = token)
    trip = trips[0]
    location_start_end = trip.location_start
    trip_description_json = json.loads(trip.trip_description_json)

    location_order_list = trip_description_json["location_order"]
    location_list = []
    location_list.append(location_start_end)
    count = 0
    
    for item in location_order_list:
        if(count == 0):
            location_list.append(item['name'])
            count += 1
        elif (count == len(location_order_list)-1):
            count += 1
        else:
            location_list.append(get_location_name_return_location_object(item["name"]))
            count += 1

    print(location_list)

    data = {
        'location_list': location_list,
    }

    return render(request, 'trip/directions&travel_modes.html', data)

# ================================ Google Map API ================================

def create(addresses_list):
    data = {}
    data['API_key'] = 'AIzaSyAaVWGBsqNsJid5C5syC3SwwfLv2nGzbq8'

    # request = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + origin_addresses + "&destinations=" + destination_addresses + "&key=" + data['API_key']
    # requests_json = requests.get(request).json()
    addresses_list.append(addresses_list[0])
    # print(addresses_list)
    
    # https://developers.google.com/maps/documentation/distance-matrix/intro
    # https://developers.google.com/maps/documentation/distance-matrix/intro#DistanceMatrixRequests
    # https://developers.google.com/maps/documentation/distance-matrix/start

    # ['กรุงเทพมหานคร', 'น้ำตกเอราวัณ', 'น้ำตกนางครวญ', 'อุทยานแห่งชาติเขาแหลม', 'กรุงเทพมหานคร']
    # print(addresses_list)
    count = 0
    request_list = []
    request_json = []
    for row_list in addresses_list:
        if(count < len(addresses_list)-1):
            per = count
            pre = count + 1
            request = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + \
                            addresses_list[per] + "&destinations=" + addresses_list[pre] + "&key=" + data['API_key']
            
            request_list.append(request)
            request_json.append(requests.get(request).json())

            if(count < len(addresses_list)):
            # if(count - 2 < len(addresses_list)):
                count += 1
 
    row_list = []
    row_list_distance = []
    row_list_duration = []

    for request in request_json:
        # print('=================================================')
        # print(request)
        row_list_distance.append(distance(request))
        row_list_duration.append(duration(request))

    row_list.append(row_list_distance)
    row_list.append(row_list_duration)

    return row_list


#===========================MAP_2===========================

def create_data(location):
    """Creates the data."""
    data = {}
    data['API_key'] = 'AIzaSyAaVWGBsqNsJid5C5syC3SwwfLv2nGzbq8'
    data['addresses'] =  location
    return data

def run_data(data):
    """Entry point of the program"""
    # Create the data.
    addresses = data['addresses']
    API_key = data['API_key']
    distance_matrix = create_distance_matrix(data)
    # print(distance_matrix)
    return distance_matrix

#===========================MAP_3===========================
def create_data_model(distance):
    """Stores the data for the problem."""
    data = {}
    data['distance_matrix'] = distance
    data['num_vehicles'] = 1
    data['depot'] = 0
    # print("------------Data------------")
    # print(data)
    return data


def run_data_model(data, distance_list):
    # output : ["location_order", "duration_all"]

    """Entry point of the program."""
    # Instantiate the data problem.
    data = create_data_model(run_data(data))

    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'], data['depot'])

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)

    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # Define cost of each arc.
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

    # Solve the problem.
    assignment = routing.SolveWithParameters(search_parameters)

    # Print solution on console.
    order = ""
    if assignment:
        plan_list = print_solution(manager, routing, assignment, distance_list)
    
    return plan_list

# ================================ End Google Map API ================================


# ================================ Function Map API ================================

def distance(request):
    distance_list = []
    distance_text = []
    distance_value = []

    for row in request['rows']:
        distance_text.append([row['elements'][0]['distance']['text']])

    for row in request['rows']:
        distance_value.append([row['elements'][0]['distance']['value']])

    distance_list.append(distance_text)
    distance_list.append(distance_value)

    return distance_list


def duration(request):
    duration_list = []
    duration_text = []
    duration_value = []

    for row in request['rows']:
        duration_text.append([row['elements'][0]['duration']['text']])

    for row in request['rows']:
        duration_value.append([row['elements'][0]['duration']['value']])


    duration_list.append(duration_text)
    duration_list.append(duration_value)

    return duration_list

# ================================ End Function Map API ================================




# ================================ Function TRIP ================================

# @login_required
# @active_required
def trip_system_save(request):

    print(request.user)

    budget = request.POST.get('budget')
    person_number = request.POST.get('person_number')
    location_start = request.POST.get('location_start')
    value_style = request.POST.get('value_style')
    type_season = request.POST.get('type_season')
    suitable_person = request.POST.get('suitable_person')


    qs = Trip.objects.filter(~Q(name = None))
    for item in qs:
        print(item)
    # results = Model.objects.exclude(a=True, x__ne=5)
    # result = read_frame(qs)
    result = read_frame(qs, fieldnames=['budget', 'location_start', 'person_number', 'style', 'type_season', 'suitable_person', 'type_adventure'])
    result['style_txt'] = result['style']
    result['type_season_txt'] = result['type_season']
    result['type_adventure_txt'] = result['type_adventure']
    result['location_start_txt'] = result['location_start']
    result['suitable_person_txt'] = result['suitable_person']


    # result_list = ['budget', 'location_start', 'person_number', 'style', 'type_season', 'suitable_person', 'type_adventure']
    print(result)
    print(list(result))

    #create a for loop to iterate through each column in the data
    # count = 0
    # count_max = len(list(result))
    for columns in list(result):
        if columns == 'location_start' or columns == 'type_season' or columns == 'suitable_person' or columns == 'style' or columns == 'type_adventure':
            encode(result[columns])
    
    print(result)

    # result_data = encode_data(result)
    # print(result_data.head())


    # split data into training and test sets; set random state to 0 for reproducibility 
    X_train, X_test, y_train, y_test = train_test_split(result[['budget', 'location_start', 'person_number', 'style', 'type_season', 'suitable_person' ]], result['type_adventure'], random_state=0)

    # see how data has been split
    print("X_train shape: {}\ny_train shape: {}".format(X_train.shape, y_train.shape))
    print("X_test shape: {}\ny_test shape: {}".format(X_test.shape, y_test.shape))


    # initialize the Estimator object
    knn = KNeighborsClassifier(n_neighbors=1)

    # fit the model to training set in order to predict classes
    knn.fit(X_train, y_train)

    # what is our score?
    print("Test set score: {:.2f}".format(knn.score(X_test, y_test)))
    test_score = knn.score(X_test, y_test)
    # answer = knn.predict(X_test)
    # print(X_test)
    # print(answer)


    style_json = result_table(result, "style", "style_txt")
    type_season_json = result_table(result, "type_season", "type_season_txt")
    location_start_json = result_table(result, "location_start", "location_start_txt")
    type_adventure_json = result_table(result, "type_adventure", "type_adventure_txt")
    suitable_person_json = result_table(result, "suitable_person", "suitable_person_txt")

    # print("==========================")
    # print(style_json)
    # print(type_season_json)
    # print(location_start_json)
    # print(type_adventure_json)
    # print(suitable_person_json)
    # print("==========================")

    # print("type_season_json: {}".format(type_season_json))
    # print("type_season: {}".format(type_season))

    # print("location_start_json: {}".format(location_start_json))
    # print("location_start: {}".format(location_start))

    value_style_new = key_value(style_json, value_style)
    type_season_new = key_value(type_season_json, type_season)
    location_start_new = key_value(location_start_json, location_start)
    suitable_person_new = key_value(suitable_person_json, suitable_person)

    answer = knn.predict(X_test)
    # print(X_test)
    # print(type_adventure_json[str(int(answer))])

    x_new = [[budget, location_start_new, person_number, value_style_new, type_season_new, suitable_person_new]]
    answer = knn.predict(x_new)
    # print(x_new)   
    print('======================================')
    print(answer)

    # score = float(answer) * 100
    type_score = type_adventure_json[str(int(answer))]
    print(type_score)

    # Text models knn
    # print(classification_report(y_train, answer))

    # type_adventure = TypeAdventure.objects.filter(id=int(answer))    

    trips = Trip.objects.filter(type_adventure=type_score)
  

    data = {
        'trips': trips,
        'type_score': type_score,
        'test_score': test_score,
    }

    # template = loader.get_template("trip/trip_list.html")
    # return HttpResponse(template.render())
    # return HttpResponseRedirect("http://example.com/")
    return render(request, "trip/trip_list.html", data)
    # return redirect('trip:trip_detail', token=token)
    # return redirect('trip:trip_system_create')


@login_required
@active_required
def system_create(request):
    styles = Style.objects.all()
    categories = CategorySub.objects.all()
    location_start_end = load_location_start_end()

    data = {
        'provinces': location_start_end,
        'styles': styles,
        'TypeAdventure': TypeAdventure, 
        'TypeSeason': TypeSeason, 
        'SuitablePerson': SuitablePerson, 
    }

    template_name = 'trip/trip_system/system_create.html'

    return render(request, template_name, data)



def trip_location(request):
    data = location_data(request)
    cartItems = data['cartItems']
    order = data['order']
    items = data['items']

    context = { 
        'items':items, 
        'order':order, 
        'cartItems':cartItems 
    }

    return render(request, 'trip/trip_person/trip_location.html', context)


def trip_summary(request):
    styles = Style.objects.all() 
    location_start_end = load_location_start_end()

    data = location_data(request)
    cartItems = data['cartItems']
    order = data['order']
    items = data['items']
    
    context = {
        'items':items, 
        'order':order, 
        'cartItems':cartItems,
        'provinces': location_start_end,
        'styles': styles,
        'TypeAdventure': TypeAdventure, 
        'TypeSeason': TypeSeason, 
        'SuitablePerson': SuitablePerson, 

    }
    return render(request, 'trip/trip_person/trip_summary.html', context)


def updateItem(request):
    body_unicode = request.body.decode('utf-8')
    data = json.loads(body_unicode) 

    locationID = data['locationID']
    action = data['action']
    print('Action:', action)
    print('Location:', locationID)
    
    customer = request.user
    location = Location.objects.get(id=locationID)
    order, created = Trip.objects.get_or_create(customer=customer, complete=False)

    orderItem, created = TripLocation.objects.get_or_create(order=order, location=location)

    if action == 'add':
        orderItem.time = (orderItem.time + 30)
    elif action == 'remove':
        orderItem.time = (orderItem.time - 30)

    orderItem.save()

    if action == 'clear':
        orderItem.delete()

    if orderItem.time <= 0:
        orderItem.delete()
    
    return JsonResponse('Item was added', safe=False)



def trip_process(request):
    language_code = translation.get_language()
    # trip_id = datetime.datetime.now().timestamp()
    print("================ trip_process ================")
    data = json.loads(request.body)
    print("================ trip_process ================")

    if request.user.is_authenticated:
        customer = request.user
        order, created = Trip.objects.get_or_create(customer=customer, complete=False)
    else:
        customer, order = guest_trip(request, data)
        del request.session['countTime']        

    token               = secrets.token_urlsafe(30)    
    total               = float(data['form']['total'])
    name                = str(data['trip']['name'])
    person_number       = str(data['trip']['person_number'])
    location_srart      = str(data['trip']['location_srart'])
    trip_styles         = str(data['trip']['trip_styles'])
    time_start          = str(data['trip']['time_start'])
    time_hotel          = str(data['trip']['time_hotel'])
    trip_description    = str(data['trip']['trip_description'])
    type_adventure      = str(data['trip']['type_adventure'])
    type_season         = str(data['trip']['type_season'])
    suitable_person     = str(data['trip']['suitable_person'])
    
    language_code = translation.get_language()

    if language_code == 'en':
        trip_styles = Style.objects.filter(id = trip_styles)[0]
    else:
        trip_styles = Style.objects.filter(id = trip_styles)[0]

    order.token                     = token
    order.name                      = name
    order.budget                    = total
    order.time_start                = time_start
    order.location_start            = location_srart
    order.person_number             = person_number
    order.style                     = trip_styles
    order.type_generate             = "User"
    order.trip_description_text     = trip_description
    order.type_adventure            = type_adventure
    order.type_season               = type_season
    order.suitable_person           = suitable_person
 
    print("---------------------------------")
    print(order.customer)
    print("---------------------------------")
    print(order.customer.email)
    print("---------------------------------")

    location_list = []
    location_time_list = []
    location_list.append(location_srart)
    test = TripLocation.objects.filter(order = order)

    for item in test:
        location_line = []
        location_list.append(item.location.name)
        location_line.append(item.location.name)
        location_line.append(item.time)
        location_time_list.append(location_line)


    # map
    data = create_data(location_list)
    plan_list = run_data_model(data, location_list)
    row_list = create(location_list)
    duration_list = row_list[1]
    distance_list = row_list[0]

    # print("data: {}".format(data))
    # print("row_list: {}".format(row_list))
    print("duration_list: {}".format(duration_list))
    print("distance_list: {}".format(distance_list))
    print("location_list: {}".format(location_list))
    print("time_start: {}".format(time_start))
    print("location_time_list: {}".format(location_time_list))
    print("time_hotel: {}".format(time_hotel))


    trip_description_json = get_data_return_json(location_list, duration_list, distance_list, time_start, location_time_list, time_hotel)

    if total == order.get_cost_total:
        order.complete = True
        order.trip_description_json = trip_description_json
    order.save()

    for item in location_list:
        # trip.tags.add(get_category_id_return_category_name(item))
        item_line = get_location_return_category(item)
        if(item_line != ""):
            order.category_sub.add(item_line)


    #send email
    context_email = {
        'trip_name': order.name,
        'token': order.token,
        'profile': order.customer,
        'hostname' : request.META['HTTP_HOST']            
    }
    email = order.customer.email
    text_trip = "You have created a trip" if language_code == 'en' else "คุณได้สร้างทริปท่องเที่ยว"
    subject = text_trip + " " + name
    send_html_email(email, subject, 'trip/email.html', context_email)


    # location_tags = []
    # for item in location_order_list:
    #     location_line1 = Location.objects.filter(name = item["name"])
    #     location_line1 = location_line1[0]

    #     for item_2 in location_line1.tags.all():
    #         location_tags.append(item_2)

    # location_tags = list(set(location_tags))
    # order.tags.add(, landmark_name, style_name)

    return JsonResponse(token, safe=False)
    # return redirect('trip:trip_detail', token=token)


def myTrip(request):
    trips = Trip.objects.filter(customer = request.user).filter(~Q(name = None))
    print(trips)
    
    context = {'trips':trips}
    return render(request, 'trip/trip_person/my_trips.html', context)

# ================================ END Function TRIP ================================
